# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

Akbar Putra Novial 1806173595

Task 1: Ensuring 12 Factors  **Done**
    
   Some of the factors that needed to be modified were the Config and Logs factors.
   For the Config factor, I needed to store the config in the environment variables. There was a sensitive information which was exposed, to solve this, we can put it inside the environment variables. 
    
   ![envvariables](images/envvariables.jpg)
    
   as for the Logs factor, Logs provide visibility into the behavior of a running app. In server-based environments they are commonly written to a file on disk (a “logfile”). for this factor, I modified the ScienceDirectPaperFinder.java. Other than those, the 12 factors are satisfied    
   
  
   
Task 2: Measuring Number of Method Invocation  **Done**
       
   To measure how many times extractKeyPhrases() method at TextAnalysisServiceImpl class is invoked, I used Prometheus. Here are some screenshots of the prometheus endpoints, metrics, and graph of extractKeyPhrases().
   ![envvariables](images/prometheus.jpg)
   
   ![envvariables](images/actuatorprometheus.jpg)
   
   ![envvariables](images/prometheusextract.jpg)
   
  
   
Task 3: Refactor Classes in the Model Layer **Done**
    
   Used project lombok annotations. Added @Data in Conference class
   
   
Task 4: Implement Data Persistence for Paper **Done**
   
   Added @Data in class Paper and changed Paper from object to Entity using @Entity and added Id with @Id. Also created PaperRepository
   
   
Task 5: Improve Code Quality **Not Done**
   
   
    
       