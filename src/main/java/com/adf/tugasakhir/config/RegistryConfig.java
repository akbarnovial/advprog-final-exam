package com.adf.tugasakhir.config;

import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RegistryConfig {

    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
        return registry ->  registry.config().commonTags("app.name","eai-group-project-2019");
    }

    @Bean
    TimedAspect timedAspect(MeterRegistry  registry) {
        return  new  TimedAspect(registry);
    }

}
